const TEMPLATE_XML = '{\n' +
    '  "actions": [\n' +
    '    {\n' +
    '      "row": [\n' +
    '        {\n' +
    '          "title": "Row one Label needs review",\n' +
    '          "action": "/label ~\\"needs review\\""\n' +
    '        },\n' +
    '        {\n' +
    '          "title": "Row one Unlabel needs review",\n' +
    '          "action": "/unlabel ~\\"needs review\\""\n' +
    '        },\n' +
    '      ],\n' +
    '      "row2": [\n' +
    '        {\n' +
    '          "title": "Row two Title one",\n' +
    '          "action": "/label ~\\"needs review\\""\n' +
    '        },\n' +
    '        {\n' +
    '          "title": "Row two Title two",\n' +
    '          "action": "/unlabel ~\\"needs review\\""\n' +
    '        },\n' +
    '      ]\n' +
    '    }\n' +
    '  ]\n' +
    '}';


function saveOptions(e) {
    browser.storage.sync.set({
        json: document.querySelector("#json").value
    });
    e.preventDefault();
}

function restoreOptions() {
    document.querySelector("#json-template").innerText = TEMPLATE_XML;


    const storageItem = browser.storage.sync.get('json');
    storageItem.then((res) => {
        if (undefined != res.json) {
            document.querySelector("#managed-json").innerText = res.json;
        }

    });

    const gettingItem = browser.storage.sync.get('json');
    gettingItem.then((res) => {
        document.querySelector("#json").value = res.json || TEMPLATE_XML;
    });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);