(
    function() {

        /**
         * Check and set a global guard variable.
         * If this content script is injected into the same page again,
         * it will do nothing next time.
         */
        if (window.hasRun) {
            return;
        }
        window.hasRun = true;

        /**
         * Remove every beast from the page.
         */
        function reset(element) {
            element.value = '';
            triggerChangeEvent(note);
        }

        function commit(element) {
            document.querySelector(".note-form-actions .js-comment-submit-button button").click();
        }
        function notes(element) {
            document.querySelector(".notes-tab a").click();
            document.querySelector(".note-form-actions").scrollIntoView();
        }

        function triggerChangeEvent(element) {
            // Create a new 'change' event
            var event = new Event('change');

            // Dispatch it.
            element.dispatchEvent(event);
        }

        /**
         * Listen for messages from the background script.
         * Call "beastify()" or "reset()".
         */
        browser.runtime.onMessage.addListener((message) => {

            let mrAuthor = document.querySelector(".author-link").getAttribute('data-username');

            let loggedUser = document.querySelector("li.current-user a").getAttribute('data-user');

            let note = document.querySelector("#note-body");

            if (message.command === "reset") {

                reset(note);

            } else if (message.command === "notes") {
                notes();
            } else if (message.command === "commit") {
                commit();
            } else if (message.command) {
                let command = message.command;

                command = command.replace('#mrAuthor#', '@' + mrAuthor);
                command = command.replace('#loggedUser#', '@' + loggedUser);

                note.value = note.value + command + '\n';
                triggerChangeEvent(note);
            }
        });

    }
)();
